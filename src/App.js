import "./App.css";
import "bootstrap/dist/css/bootstrap.min.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import { Container } from "react-bootstrap";
import Category from "./pages/Category";
import 'moment/locale/km';


function App() {
    return (
        <BrowserRouter>
            <Container>
                <Switch>
                    <Route path="/category" component={Category} />
                </Switch>
            </Container>
        </BrowserRouter>
    );
}

export default App;
