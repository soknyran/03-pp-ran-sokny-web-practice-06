import axios from "axios";

//Base URL
export const API = axios.create({
    baseURL: "http://110.74.194.124:3034/api",
});

//fetch all articles
export const fetchAllArticles = async () => {
    try {
        const result = await API.get("/articles");
        console.log("fetchAllArticles:", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("fetchAllArticles Error:", error);
    }
};



//fetch article by id
export const fetchArticleById = async (id) => {
    try {
        const result = await API.get(`/articles/${id}`)
        console.log("fetchArticleById:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("fetchArticleById error:", error);
    }
}


//delete article by ID
export const deleteAritcleById = async (id) => {
    try {
        const result = await API.delete(`/articles/${id}`);
        console.log("deleteAritcleById:", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("deleteAritcleById Error:", error);
    }
}

//upload image to api
export const uploadImage = async (image) => {
    try {
        const fd = new FormData();
        fd.append("image", image)
        const result = await API.post("/images", fd, {
            onUploadProgress: progress => {
                console.log("Uploaded:", Math.floor(progress.loaded / progress.total * 100));
            }
        })
        console.log("uploadImage:", result.data.url);
        return result.data.url
    } catch (error) {
        console.log("uploadImage Error:", error);
    }
}


//Add article to api
export const addArticle = async (article) => {
    try {
        const result = await API.post("/articles", article);
        console.log("addArticle:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("addArticle Error:", error);
    }
}

//update article by ID
export const updateArticleById = async (id, article) => {
    try {
        const result = await API.patch(`/articles/${id}`, article)
        console.log("updateArticleById:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("updateArticleById Error:", error);
    }
}



//fetch all categories
export const fetchAllCategories = async () => {
    try {
        const result = await API.get("/category");
        console.log("fetchAllCategories:", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("fetchAllCategories Error:", error);
    }
};

//fetch all categories by id
export const fetchCategoryById = async (id) => {
    try {
        const result = await API.get(`/category/${id}`)
        console.log("fetchCategoryById:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("fetchCategoryById error:", error);
    }
}


//delete category by ID
export const deleteCategoryById = async (id) => {
    try {
        const result = await API.delete(`/category/${id}`);
        console.log("deleteCategoryById:", result.data.data);
        return result.data.data;
    } catch (error) {
        console.log("deleteCategoryById Error:", error);
    }
}


//Add article to api
export const addCategory = async (category) => {
    try {
        const result = await API.post("/category", category);
        console.log("addCategory:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("addCategory Error:", error);
    }
}

//update article by ID
export const updateCategoryById = async (id, category) => {
    try {
        const result = await API.put(`/category/${id}`, category)
        console.log("updateCategoryById:", result.data.data);
        return result.data.data
    } catch (error) {
        console.log("updateCategoryById Error:", error);
    }
}